# Parsley / Spicelib binary build #

This is a binary version of [Parsley-Spicelib-complete](https://bitbucket.org/borekb/parsley-spicelib-complete) built with Adobe Flex 4.6.0 (works with newer Apache SDK projects without problems).

Branches in this repository match branches in the original source repository. 